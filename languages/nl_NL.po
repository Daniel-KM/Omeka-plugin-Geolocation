# Translation for the Geolocation plugin for Omeka.
# Copyright (C) 2011 Roy Rosenzweig Center for History and New Media
# This file is distributed under the same license as the Omeka package.
# 
# Translators:
# hans schraven <hans.schraven@gmail.com>, 2015
# John Flatness <john@zerocrates.org>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Omeka\n"
"Report-Msgid-Bugs-To: http://github.com/omeka/plugin-Geolocation/issues\n"
"POT-Creation-Date: 2012-01-09 21:49-0500\n"
"PO-Revision-Date: 2016-09-22 18:19+0000\n"
"Last-Translator: John Flatness <john@zerocrates.org>\n"
"Language-Team: Dutch (Netherlands) (http://www.transifex.com/omeka/omeka/language/nl_NL/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: nl_NL\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: GeolocationPlugin.php:235 GeolocationPlugin.php:253
msgid "Geolocation"
msgstr "Geo-locatie"

#: GeolocationPlugin.php:364
msgid "kilometers"
msgstr "kilometers"

#: GeolocationPlugin.php:366
msgid "miles"
msgstr "mijlen"

#: GeolocationPlugin.php:368
#, php-format
msgid "within %1$s %2$s of \"%3$s\""
msgstr "binnen %1$s %2$s van \"%3$s\""

#: GeolocationPlugin.php:388 GeolocationPlugin.php:394
msgid "Map"
msgstr "Kaart"

#: GeolocationPlugin.php:427
msgid "Browse Map"
msgstr "Doorzoek kaart"

#: GeolocationPlugin.php:482
#, php-format
msgid "Find A Geographic Location For The %s:"
msgstr "Een geografische locatie vinden voor de %s:"

#: GeolocationPlugin.php:489
msgid "Geolocation Map"
msgstr "Geolocatiekaart"

#: GeolocationPlugin.php:490
msgid "Show attached items on a map"
msgstr "Toon bijgesloten items op een kaart"

#: GeolocationPlugin.php:589
msgid "Find a Location by Address:"
msgstr "Vind een locatie door middel van een adres"

#: config_form.php:3
msgid "General Settings"
msgstr "Algemene instellingen"

#: config_form.php:7
msgid "API Key"
msgstr ""

#: config_form.php:12
#, php-format
msgid "Google API key for this project. For more information, see %s."
msgstr ""

#: config_form.php:23
msgid "Default Latitude"
msgstr "Standaard breedtegraad"

#: config_form.php:26
msgid ""
"Latitude of the map's initial center point, in degrees. Must be between -90 "
"and 90."
msgstr "De breedtegraad van het oorspronkelijke centrum van de kaart in graden. Deze moet tussen de -90 en 90 liggen."

#: config_form.php:33
msgid "Default Longitude"
msgstr "Standaard lengtegraad"

#: config_form.php:36
msgid ""
"Longitude of the map's initial center point, in degrees. Must be between "
"-180 and 180."
msgstr "De lengtegraad van het oorspronkelijke centrum van de kaart in graden. Deze moet tussen de -180 en 180 liggen."

#: config_form.php:43
msgid "Default Zoom Level"
msgstr "Standaard zoomniveau"

#: config_form.php:46
msgid ""
"An integer greater than or equal to 0, where 0 represents the most zoomed "
"out scale."
msgstr "Een geheel getal dat groter of gelijk is aan 0, waarbij 0 staat voor het verste uitgezoomd."

#: config_form.php:53
msgid "Map Type"
msgstr "Soort kaart"

#: config_form.php:56
msgid "The type of map to display"
msgstr "Het soort weer te geven kaart"

#: config_form.php:59
msgid "Roadmap"
msgstr "Wegenkaart"

#: config_form.php:60
msgid "Satellite"
msgstr "Satelliet"

#: config_form.php:61
msgid "Hybrid"
msgstr "Beide"

#: config_form.php:62
msgid "Terrain"
msgstr "Terrein"

#: config_form.php:71
msgid "Browse Map Settings"
msgstr "Kaartinstellingen doorzoeken"

#: config_form.php:74
msgid "Number of Locations Per Page"
msgstr "Aantal locaties per pagina"

#: config_form.php:77
msgid "The number of locations displayed per page when browsing the map."
msgstr "Het aantal locaties dat per pagina wordt weergegeven als u de kaart doorzoekt."

#: config_form.php:83
msgid "Auto-fit to Locations"
msgstr "Automatisch aanpassen aan locaties"

#: config_form.php:87
msgid ""
"If checked, the default location and zoom settings will be ignored on the "
"browse map. Instead, the map will automatically pan and zoom to fit the "
"locations displayed on each page."
msgstr "Als deze keuze is aangevinkt, worden de standaard locatie- en zoominstellingen op de zoekkaart genegeerd. In plaats daarvan verschuift de kaart en wordt er in- of uitgezoomd zodat de locaties die op elke pagina worden weergegeven passen."

#: config_form.php:101
msgid "Default Radius"
msgstr "Standaard straal"

#: config_form.php:104
msgid ""
"The size of the default radius to use on the items advanced search page. See"
" below for whether to measure in miles or kilometers."
msgstr "De afmeting van de standaard straal die op de geavanceerde zoekpagina voor items moet worden gebruikt. Hieronder kunt u instellen of afstanden in mijlen of kilometers moeten worden weergegeven."

#: config_form.php:110
msgid "Use metric distances"
msgstr "Gebruik het metrisch stelsel voor afstanden."

#: config_form.php:113
msgid "Use metric distances in proximity search."
msgstr "Gebruik het metrisch stelsel voor zoeken in de omgeving."

#: config_form.php:123
msgid "Item Map Settings"
msgstr "Kaartinstellingen item"

#: config_form.php:126
msgid "Width for Item Map"
msgstr "Breedte voor itemkaart"

#: config_form.php:129
msgid ""
"The width of the map displayed on your items/show page. If left blank, the "
"default width of 100% will be used."
msgstr "De breedte van de kaart die op uw items/toon-pagina wordt weergegeven. Als deze waarde niet wordt ingevuld, de standaard breedte van 100% worden gebruikt."

#: config_form.php:136
msgid "Height for Item Map"
msgstr "Hoogte voor itemkaart"

#: config_form.php:139
msgid ""
"The height of the map displayed on your items/show page. If left blank, the "
"default height of 300px will be used."
msgstr "De hoogte van de kaart die op uw items/toon-pagina wordt weergegeven. Als deze waarde niet wordt ingevuld, de standaard hoogte van 300px worden gebruikt."

#: config_form.php:146
msgid "Map Integration"
msgstr "Neem in kaart op"

#: config_form.php:149
msgid "Add Link to Map on Items/Browse Navigation"
msgstr "Voeg een link toe aan Kaart bij Items/Bladeren"

#: config_form.php:152
msgid "Add a link to the items map on all the items/browse pages."
msgstr "Hiermee voegt u een link toe aan de itemskaart op alle items-/zoekpagina's."

#: config_form.php:162
msgid "Add Map To Contribution Form"
msgstr "Voeg een kaart aan een bijdrageformulier toe"

#: config_form.php:165
msgid ""
"If the Contribution plugin is installed and activated, Geolocation  will add"
" a geolocation map field to the contribution form to associate a location to"
" a contributed item."
msgstr "Als de plug-in Bijdragen is geïnstalleerd en geactiveerd, dan voegt Geolocatie een geolocatiekaartveld toe aan het bijdrageformulier om een locatie aan een bijgedragen item te koppelen."

#: models/Location.php:22
msgid "Location requires an item ID."
msgstr "Voor de locatie is een item-ID nodig."

#: models/Location.php:26
msgid "Location requires a valid item ID."
msgstr "Voor de locatie is een geldig item-ID nodig."

#: models/Location.php:31
msgid "A location already exists for the provided item."
msgstr "Er bestaat al een locatie voor het aangeboden item."

#: models/Location.php:34
msgid "Location requires a latitude."
msgstr "Geef een breedtegraad op voor de locatie."

#: models/Location.php:37
msgid "Location requires a longitude."
msgstr "Geef een lengtegraad op voor de locatie."

#: models/Location.php:40
msgid "Location requires a zoom level."
msgstr "Geef een zoomniveau op voor de locatie."

#: views/admin/map/browse.php:4 views/public/map/browse.php:4
msgid "Browse Items on the Map"
msgstr "Blader door items op de kaart"

#: views/admin/map/browse.php:4
msgid "total"
msgstr "totaal"

#: views/admin/map/browse.php:13 views/public/map/browse.php:21
msgid "Find An Item on the Map"
msgstr "Zoek een item op de kaart"

#: views/helpers/ItemGoogleMap.php:36
msgid "This item has no location info associated with it."
msgstr "Er is geen locatie aan dit item gekoppeld."

#: views/shared/map/advanced-search-partial.php:16
msgid "Geographic Radius (kilometers)"
msgstr "Geografische radius (in kilometers)"

#: views/shared/map/advanced-search-partial.php:18
msgid "Geographic Radius (miles)"
msgstr "Geografische radius (in mijlen)"

#: views/shared/map/advanced-search-partial.php:25
msgid "Geographic Address"
msgstr "Geografisch adres"

#: views/shared/map/input-partial.php:7
msgid "Find"
msgstr "Zoeken"
